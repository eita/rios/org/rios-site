# Mapa do Site

Rios de Informações OrganizadaS

## Definições para contrução do release

O Rios é um/a:
* App
* Sistema
* Ferramenta
* Ecossistema
* Plataforma
* Solução

Objetivamente, o Rios faz/facilita:
* Trabalho Coletivo/Colaborativo
* Sistematização
* Conversas e arquivos num mesmo lugar

O Rios oferece como valores:
* Soberania e segurança de dados
* Privacidade
* Aproximar Experiência e Sistematização

O Rios revolve os seguintes problemas:
* Dados desorganizados
* Conversas e arquivos que se perdem
* Espionagem de governos e agências de segurança

O Rios é composto por:
* Ferramentas
* Apps
* Módulos

O Rios/ A plataforma Rios / O Ecossistema Rios / O sistema Rios / O App Rios é desenvolvido pela Cooperativa EITA, que há 5 anos busca colocar tecnologias da informação e comunicação nas mãos dos movimentos sociais. A Cooperativa CanTrust é parceira de EITA neste projeto, e promove a hospedagem, segurança e backups dos servidores do Rios.

## Elementos do Site

* O que é o Rios
* O que o Rio oferece (problema e solução)
* Quem já usa o Rios
* Quem é reponsável pelo Rios
* Como acessar/demo/screenshots
* Quanto custa
